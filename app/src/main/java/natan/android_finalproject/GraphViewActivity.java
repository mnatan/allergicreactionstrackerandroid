package natan.android_finalproject;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

public class GraphViewActivity extends AppCompatActivity {

    final ArrayList<ContactEntryData> list = new ArrayList<>();
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view);

        final AlergicDbHelper mDbHelper = new AlergicDbHelper(this);
        db = mDbHelper.getReadableDatabase();
        String query = " SELECT * FROM inputs ORDER BY time ASC; ";

        Cursor cursor = db.rawQuery(
                query,
                null
        );

        if (cursor != null) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                list.add(
                        new ContactEntryData(
                                cursor.getInt(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry._ID
                                        )
                                ),
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TYPE
                                        )
                                ),
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_DESCRIPTION
                                        )
                                ),
                                cursor.getInt(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_COMFORT
                                        )
                                ),
                                cursor.getLong(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TIME
                                        )
                                )
                        )
                );
            }
            cursor.close();
        }

        GraphView graph = (GraphView) findViewById(R.id.graph);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>();
        for (ContactEntryData tmp: list) {
            series.appendData(new DataPoint(tmp.getTime(),tmp.getComfort()), true, 10);
        }
        graph.addSeries(series);

    }
}
