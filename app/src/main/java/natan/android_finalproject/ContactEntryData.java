package natan.android_finalproject;

import android.content.Context;

import java.text.SimpleDateFormat;

/**
 * Created by natan on 20.02.16.
 */
public class ContactEntryData {

    public int id;
    public String inputtype;

    public int getId() {
        return id;
    }

    public String desc;
    public int comfort;
    public long time;

    public ContactEntryData(int id, String inputtype, String desc, int comfort, long time) {
        this.inputtype = inputtype;
        this.desc = desc;
        this.comfort = comfort;
        this.time = time;
        this.id = id;
    }

    public String getInputtype() {
        return inputtype;
    }

    public String getDesc() {
        return desc;
    }

    public int getComfort() {
        return comfort;
    }

    public String getTime(Context context) {
        SimpleDateFormat timeFormat = new SimpleDateFormat(context.getString(R.string.date_time_format));
        return timeFormat.format(time);
    }

    public long getTime() {
        return time;
    }

}
