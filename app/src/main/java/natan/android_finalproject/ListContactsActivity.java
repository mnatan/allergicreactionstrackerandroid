package natan.android_finalproject;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListContactsActivity extends AppCompatActivity {

    final ArrayList<ContactEntryData> list = new ArrayList<>();
    ListView listview;
    Context mContext;
    SQLiteDatabase db;
    int ActivityReactionID;
    String ActivityReactionDesc;
    long ActivityReactionTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contacts);
        mContext = this;
        ActivityReactionID = getIntent().getIntExtra("ID", 0);
        ActivityReactionDesc = getIntent().getStringExtra("DESC");
        ActivityReactionTime = getIntent().getLongExtra("TIME",0);

        listview = (ListView)findViewById(R.id.contactsListView);
        TextView title = (TextView)findViewById(R.id.contactsTitleTextView);
        title.setText("Contacts for Reaction '" + ActivityReactionDesc + "':");

        final AlergicDbHelper mDbHelper = new AlergicDbHelper(this);
        db = mDbHelper.getReadableDatabase();

        String query = " SELECT * FROM inputs " +
                " WHERE " + AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TYPE + " = 'Contact' " +
                " AND " +
                " time > ((select time from inputs where "
                + AlergicDatabaseContract.ContactEntry._ID + " = " + ActivityReactionID + ") - 1000*60*60*24)" +
                " AND time <= (select time from inputs where "
                + AlergicDatabaseContract.ContactEntry._ID+ " = " +ActivityReactionID + ")"
                + " ORDER BY "+AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TIME +" DESC;";

        Cursor cursor = db.rawQuery(
                query,
                null
        );

        if (cursor != null) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                list.add(
                        new ContactEntryData(
                                cursor.getInt(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry._ID
                                        )
                                ),
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TYPE
                                        )
                                ),
                                cursor.getString(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_DESCRIPTION
                                        )
                                ),
                                cursor.getInt(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_COMFORT
                                        )
                                ),
                                cursor.getLong(
                                        cursor.getColumnIndex(
                                                AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TIME
                                        )
                                )
                        )
                );
            }
            cursor.close();
        }

        final ListContactsArrayAdapter adapter = new ListContactsArrayAdapter(
                this, R.layout.reactions_list_item, list
        );

        listview.setAdapter(adapter);

        db.close();
    }

    private class ListContactsArrayAdapter extends ArrayAdapter<ContactEntryData> {

        ArrayList<ContactEntryData> list = new ArrayList<>();
        int ResourceId;
        Context mContext;

        public ListContactsArrayAdapter(Context context, int textViewResourceId,
                                        List<ContactEntryData> objects) {
            super(context, textViewResourceId, objects);
            ResourceId = textViewResourceId;
            mContext = context;
            list = (ArrayList<ContactEntryData>) objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(ResourceId, parent, false);
            }

            // object item based on the position
            ContactEntryData entry = list.get(position);

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem;
            textViewItem = (TextView) convertView.findViewById(R.id.reactions_list_item_text);
            textViewItem.setText(entry.getDesc());
            textViewItem = (TextView) convertView.findViewById(R.id.date_value);
            textViewItem.setText(entry.getTime(mContext));
            textViewItem = (TextView) convertView.findViewById(R.id.comfort_value);
            textViewItem.setText(""+entry.getComfort());
            textViewItem = (TextView) convertView.findViewById(R.id.ago);
            long time = (ActivityReactionTime - entry.getTime() ) / (60*1000);
            if (time > 60){
                time /= 60;
                textViewItem.setText(  time + "h ");
            } else {
                textViewItem.setText(  time + "min ");
            }

            return convertView;
        }

        @Override
        public void remove(ContactEntryData object) {
            super.remove(object);
        }
    }
}
