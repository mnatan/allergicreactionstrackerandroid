package natan.android_finalproject;

import android.app.Dialog;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddNoteActivity extends AppCompatActivity {

    SimpleDateFormat timeFormat;

    Date noteTime = new Date();
    EditText description;
    Spinner spinner;
    RatingBar comfort;
    Button datetime_btn;

    Dialog datetime_popup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        datetime_btn = (Button) findViewById(R.id.datetime_button);
        spinner = (Spinner) findViewById(R.id.type_of_entry_spinner);
        description = (EditText) findViewById(R.id.note_input);
        comfort = (RatingBar) findViewById(R.id.ratingBar);
        timeFormat = new SimpleDateFormat(getString(R.string.date_time_format));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.types_of_entry, android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        String currdate = timeFormat.format(noteTime);
        datetime_btn.setText(currdate);

        datetime_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datetime_popup = new Dialog(AddNoteActivity.this);

                datetime_popup.setContentView(R.layout.date_time_picker);
                datetime_popup.setTitle(R.string.choose_date_and_time);
                Button dialog_button = (Button) datetime_popup.findViewById(R.id.dialog_ok);
                final DatePicker date_picker = (DatePicker) datetime_popup.findViewById(R.id.datePicker1);
                final TimePicker time_picker = (TimePicker) datetime_popup.findViewById(R.id.timePicker1);
                time_picker.setIs24HourView(true);
                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noteTime = getDateFromPickers(date_picker, time_picker).getTime();
                        String currdate = timeFormat.format(noteTime);
                        datetime_btn.setText(currdate);
                        datetime_popup.dismiss();
                    }
                });

                datetime_popup.show();
            }
        });
    }

    public Calendar getDateFromPickers(DatePicker datePicker, TimePicker timePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();
        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minute);

        return calendar;
    }

    public void saveInDb(View view) {
        AlergicDbHelper mDbHelper = new AlergicDbHelper(this);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        if (description.getText().toString().equals("")){return;}
        if ((int)comfort.getRating() == 0){return;}

        ContentValues values = new ContentValues();
        values.put(AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TYPE, spinner.getSelectedItem().toString());
        values.put(AlergicDatabaseContract.ContactEntry.COLUMN_NAME_DESCRIPTION, description.getText().toString());
        values.put(AlergicDatabaseContract.ContactEntry.COLUMN_NAME_COMFORT, (int)comfort.getRating());
        int seconds = (int) System.currentTimeMillis()/1000;
        values.put(AlergicDatabaseContract.ContactEntry.COLUMN_NAME_TIME, noteTime.getTime());
        Log.d("timedebug", timeFormat.format(noteTime.getTime()));
        db.insert(AlergicDatabaseContract.ContactEntry.TABLE_NAME, null, values);

        this.finish();
    }
}
