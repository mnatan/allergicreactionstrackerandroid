package natan.android_finalproject;

import android.provider.BaseColumns;

public final class AlergicDatabaseContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public AlergicDatabaseContract() {}

    /* Inner class that defines the table contents */
    public static abstract class ContactEntry implements BaseColumns {
        public static final String TABLE_NAME = "inputs";
        public static final String COLUMN_NAME_TYPE = "inputtype";
        public static final String COLUMN_NAME_DESCRIPTION = "desc";
        public static final String COLUMN_NAME_COMFORT = "comfort";
        public static final String COLUMN_NAME_TIME = "time";


    }
}