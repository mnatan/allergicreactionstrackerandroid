package natan.android_finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);


        Button mClickButton1 = (Button)findViewById(R.id.add_note_button);
        mClickButton1.setOnClickListener(this);
        Button mClickButton2 = (Button)findViewById(R.id.list_notes_button);
        mClickButton2.setOnClickListener(this);
        Button mClickButton3 = (Button)findViewById(R.id.graph_view_button);
        mClickButton3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case  R.id.add_note_button: {
                Intent myIntent = new Intent(MainMenuActivity.this, AddNoteActivity.class);
                MainMenuActivity.this.startActivity(myIntent);
                break;
            }
            case R.id.list_notes_button: {
                Intent myIntent = new Intent(MainMenuActivity.this, ListReactionsActivity.class);
                MainMenuActivity.this.startActivity(myIntent);
                break;
            }
            case R.id.graph_view_button: {
                Intent myIntent = new Intent(MainMenuActivity.this, GraphViewActivity.class);
                MainMenuActivity.this.startActivity(myIntent);
                break;
            }
        }
    }
}