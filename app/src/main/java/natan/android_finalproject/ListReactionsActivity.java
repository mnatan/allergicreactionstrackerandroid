package natan.android_finalproject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import natan.android_finalproject.AlergicDatabaseContract.ContactEntry;

import java.util.ArrayList;
import java.util.List;

public class ListReactionsActivity extends AppCompatActivity {

    final ArrayList<ContactEntryData> list = new ArrayList<>();
    Context mContext;
    ListView listview;
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_reactions);
        mContext = this;

        listview = (ListView)findViewById(R.id.reactionsListView);

        final AlergicDbHelper mDbHelper = new AlergicDbHelper(this);
        db = mDbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery(
                "SELECT * FROM inputs " +
                        "WHERE " + ContactEntry.COLUMN_NAME_TYPE + " LIKE 'Reaction';",
                null
        );

        if (cursor != null) {
            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                list.add(
                        new ContactEntryData(
                            cursor.getInt(
                                    cursor.getColumnIndex(
                                            ContactEntry._ID
                                    )
                            ),
                            cursor.getString(
                                    cursor.getColumnIndex(
                                            ContactEntry.COLUMN_NAME_TYPE
                                    )
                            ),
                            cursor.getString(
                                    cursor.getColumnIndex(
                                            ContactEntry.COLUMN_NAME_DESCRIPTION
                                    )
                            ),
                            cursor.getInt(
                                    cursor.getColumnIndex(
                                            ContactEntry.COLUMN_NAME_COMFORT
                                    )
                            ),
                            cursor.getLong(
                                    cursor.getColumnIndex(
                                            ContactEntry.COLUMN_NAME_TIME
                                    )
                            )
                        )
                );
            }
            cursor.close();
        }

        final ListReactionsArrayAdapter adapter = new ListReactionsArrayAdapter(
                this, R.layout.reactions_list_item, list
        );

        listview.setAdapter(adapter);

        db.close();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    int position, long id) {

                final ContactEntryData item = (ContactEntryData) parent.getItemAtPosition(position);

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                Intent myIntent = new Intent(ListReactionsActivity.this, ListContactsActivity.class);
                                myIntent.putExtra("ID", item.getId());
                                myIntent.putExtra("DESC", item.getDesc());
                                myIntent.putExtra("TIME", item.getTime());
                                ListReactionsActivity.this.startActivity(myIntent);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                break;

                            case DialogInterface.BUTTON_NEUTRAL:
                                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                db = mDbHelper.getWritableDatabase();
                                                db.execSQL(
                                                        "DELETE FROM " + ContactEntry.TABLE_NAME +
                                                                " WHERE " + ContactEntry._ID + " = " + item.getId() + ";"
                                                );
                                                adapter.remove(item);
                                                adapter.notifyDataSetChanged();
                                                db.close();
                                                break;

                                            case DialogInterface.BUTTON_NEGATIVE:
                                                //No button clicked
                                                break;
                                        }
                                    }
                                };

                                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                                builder.setMessage(
                                        getString(R.string.do_you_want_to_remove)
                                                + item.getDesc() + "'?")
                                        .setPositiveButton(getString(R.string.yes), dialogClickListener)
                                        .setNegativeButton(getString(R.string.no), dialogClickListener).show();
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(
                        "What do you want to do with '"
                                + item.getDesc() + "'?")
                        .setPositiveButton("List Contacts", dialogClickListener)
                        .setNeutralButton("Remove", dialogClickListener)
                        .setNegativeButton("Nothing", dialogClickListener).show();
            }
        });
    }

    private class ListReactionsArrayAdapter extends ArrayAdapter<ContactEntryData> {

        ArrayList<ContactEntryData> list = new ArrayList<>();
        int ResourceId;
        Context mContext;

        public ListReactionsArrayAdapter(Context context, int textViewResourceId,
                                         List<ContactEntryData> objects) {
            super(context, textViewResourceId, objects);
            ResourceId = textViewResourceId;
            mContext = context;
            list = (ArrayList<ContactEntryData>) objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView==null){
                // inflate the layout
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(ResourceId, parent, false);
            }

            // object item based on the position
            ContactEntryData entry = list.get(position);

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem;
            textViewItem = (TextView) convertView.findViewById(R.id.reactions_list_item_text);
            textViewItem.setText(entry.getDesc());
            textViewItem = (TextView) convertView.findViewById(R.id.date_value);
            textViewItem.setText(entry.getTime(mContext));
            textViewItem = (TextView) convertView.findViewById(R.id.comfort_value);
            textViewItem.setText(""+entry.getComfort());

            return convertView;
        }

        @Override
        public void remove(ContactEntryData object) {
            super.remove(object);
        }
    }

}
