DROP TABLE IF EXISTS inputs;
CREATE TABLE inputs(
        id           INTEGER PRIMARY KEY AUTOINCREMENT,
        inputtype    TEXT NOT NULL,
        opis         TEXT NOT NULL,
        humor        INTEGER NOT NULL,
        time         INTEGER DEFAULT (strftime('%s','now')) NOT NULL
        );

INSERT INTO inputs (inputtype, opis, humor, time) 
    VALUES("contact", "kot", 3, strftime('%s','now') - 60*60*25);
INSERT INTO inputs (inputtype, opis, humor, time) 
    VALUES("contact", "jabłko", 3, strftime('%s','now') - 60*60);
INSERT INTO inputs (inputtype, opis, humor, time) 
    VALUES("contact", "banan", 3, strftime('%s','now') - 60*50);
INSERT INTO inputs (inputtype, opis, humor) 
    VALUES("reaction", "wysypka", 1);

select * from inputs;
select "hehe";
#select * from inputs 
#    where time > 
#        ((select time from inputs where id = 4) - 60*60*24)
#    and time <
#        (select time from inputs where id = 4);
delete from inputs where id = 1;
select * from inputs;

SELECT * FROM inputs 
    WHERE 
        time > ((select time from inputs where _id = 14) - 60*60*24)   
    and 
        time < (select time from inputs where _id = 14)
